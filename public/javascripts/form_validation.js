function checkOrderFormError(e){
if(e == 1){
    return 1;
}
else {
    return 0;
}
}


function validatePhone(phone){
    if(phone.val().length < 10){
        phone.parent().addClass('form-input-validation-error');
        error = 1;
    }
    else {
        phone.parent().removeClass('form-input-validation-error');
        error = checkOrderFormError(error);
    }
    return error;  
}

$( document ).ready(function() {
  /*MASK FOR PHONE*/
  //$("input[name='content[phone]']").mask("+9(999)999-99-99");

  //phone, email, checkbox
  $('.js-form-dealer-yellow, .js-shop-form, .js-premium-form, .js-faq-form').submit(function(e){
    //e.preventDefault();
    var error = 0;

    var phone = $(this).find('[name="content[phone]"]');
    if(phone.val().length < 10){
        phone.parent().addClass('form-input-validation-error');
        error = 1;
    }
    else {
        phone.parent().removeClass('form-input-validation-error');
        error = checkOrderFormError(error);
    }

    var email = $(this).find('[name="content[email]"]');
    if(validateEmail(email.val())){
        email.parent().removeClass('form-input-validation-error');
        error = checkOrderFormError(error);
    }
    else {
        email.parent().addClass('form-input-validation-error');
        error = 1;
    }

    var conf = $(this).find('[type="checkbox"]');
    if(conf.is(':checked') == true){
        conf.parent().removeClass('form-input-validation-error');
        error = checkOrderFormError(error);
    }
    else {
        conf.parent().addClass('form-input-validation-error');
        error = 1;
    }

    if(error == 1){
        return false;
    }
  })


  // phone, checkbox
  $('.js-premium-form-short, .delivery-form_form').submit(function(e){
    //e.preventDefault();
    var error = 0;

    var phone = $(this).find('[name="content[phone]"]');
    if(phone.val().length < 10){
        phone.parent().addClass('form-input-validation-error');
        error = 1;
    }
    else {
        phone.parent().removeClass('form-input-validation-error');
        error = checkOrderFormError(error);
    }

    var conf = $(this).find('[type="checkbox"]');
    if(conf.is(':checked') == true){
        conf.parent().removeClass('form-input-validation-error');
        error = checkOrderFormError(error);
    }
    else {
        conf.parent().addClass('form-input-validation-error');
        error = 1;
    }

    if(error == 1){
        return false;
    }
  })

  // name, phone, checkbox
  $('.js-feedback-form-modal, .js-best-price-popup__form').submit(function(e){
    //e.preventDefault();
    var error = 0;

    var name = $(this).find('[name="content[name]"]');
    if(name.val().length < 1){
        name.parent().addClass('form-input-validation-error');
        error = 1;
    }
    else {
        name.parent().removeClass('form-input-validation-error');
        error = checkOrderFormError(error);
    }

    var phone = $(this).find('[name="content[phone]"]');
    if(phone.val().length < 10){
        phone.parent().addClass('form-input-validation-error');
        error = 1;
    }
    else {
        phone.parent().removeClass('form-input-validation-error');
        error = checkOrderFormError(error);
    }

    var conf = $(this).find('[type="checkbox"]');
    if(conf.is(':checked') == true){
        conf.parent().removeClass('form-input-validation-error');
        error = checkOrderFormError(error);
    }
    else {
        conf.parent().addClass('form-input-validation-error');
        error = 1;
    }

    if(error == 1){
        return false;
    }
  })

  //phone, email, textarea, checkbox
  $('.js-faq-form').submit(function(e){
    //e.preventDefault();
    var error = 0;

    var phone = $(this).find('[name="content[phone]"]');
    if(phone.val().length < 10){
        phone.parent().addClass('form-input-validation-error');
        error = 1;
    }
    else {
        phone.parent().removeClass('form-input-validation-error');
        error = checkOrderFormError(error);
    }

    var email = $(this).find('[name="content[email]"]');
    if(validateEmail(email.val())){
        email.parent().removeClass('form-input-validation-error');
        error = checkOrderFormError(error);
    }
    else {
        email.parent().addClass('form-input-validation-error');
        error = 1;
    }

    var textarea = $(this).find('[name="content[title]"]');
    if(textarea.val().length < 10){
        textarea.parent().addClass('form-input-validation-error');
        error = 1;
    }
    else {
        textarea.parent().removeClass('form-input-validation-error');
        error = checkOrderFormError(error);
    }


    var conf = $(this).find('[type="checkbox"]');
    if(conf.is(':checked') == true){
        conf.parent().removeClass('form-input-validation-error');
        error = checkOrderFormError(error);
    }
    else {
        conf.parent().addClass('form-input-validation-error');
        error = 1;
    }

    if(error == 1){
        return false;
    }
  })

});

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
;


function jsValidCardForm(e) {
    //e.preventDefault();
    var error = 0;
    var form = $('#card_form');
    var name = form.find('[name="content[name]"]');
    if(name.val().length < 1){
        name.parent().addClass('form-input-validation-error-cart');
        error = 1;
    }
    else {
        name.parent().removeClass('form-input-validation-error-cart');
        error = checkOrderFormError(error);
    }

    var phone = form.find('[name="content[phone]"]');
    if(phone.val().length < 10){
        phone.parent().addClass('form-input-validation-error-cart');
        error = 1;
    }
    else {
        phone.parent().removeClass('form-input-validation-error-cart');
        error = checkOrderFormError(error);
    }

    var email = form.find('[name="content[email]"]');
    if(validateEmail(email.val())){
        email.parent().removeClass('form-input-validation-error-cart');
        error = checkOrderFormError(error);
    }
    else {
        email.parent().addClass('form-input-validation-error-cart');
        error = 1;
    }

    var address = form.find('[name="content[address]"]');
    if(address.val().length < 10){
        address.parent().addClass('form-input-validation-error-cart');
        error = 1;
    }
    else {
        address.parent().removeClass('form-input-validation-error-cart');
        error = checkOrderFormError(error);
    }

    if(error == 1){
        return false;
    } else {
        return true;
    }
}
;
