$( document ).ready(function() {

  let handle1 = $( "#dealers-handle-1" );
  let handle2 = $( "#dealers-handle-2" );
  let value1 = 0;
  let value2 = 0;



  $( "#dealers-slider-1" ).slider({
    value: 342748,
    min: 40000,
    max: 550000,
    step: 1,
    create: function() {
      value1 = $( "#dealers-slider-1" ).slider("option", "value");
    },
    slide: function( event, ui ){
      value1 = ui.value;
      $('#dealers-slider-1-value').text(numberFormat(ui.value, ' '));
      $('#dealers-profit-total').text(numberFormat(((ui.value * value2) * 0.3), ' ') + 'Р.');
    }
  });
  $( "#dealers-slider-2" ).slider({
    value: 17,
    min: 1,
    max: 40,
    step: 1,
    create: function() {
      value2 = $( "#dealers-slider-2" ).slider("option", "value");
    },
    slide: function( event, ui ){
      value2 = ui.value;
      $('#dealers-slider-2-value').text(ui.value);
      $('#dealers-profit-total').text(numberFormat(((ui.value * value1) * 0.3), ' ') + 'Р.');
    }
  });


  $('#dealers-landing-text').on('afterChange', function(event, slick, direction){
    let mobile_id_1 = $('#dlmn-1');
    let mobile_id_2 = $('#dlmn-2');
    if (mobile_id_1.hasClass('active')) {
      mobile_id_1.removeClass('active');
      mobile_id_2.addClass('active');
    } else {
      mobile_id_2.removeClass('active');
      mobile_id_1.addClass('active');
    }

  });

  $('#dealers-landing-text').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 2000,
    responsive: [
        {
            breakpoint: 9999,
            settings: "unslick"
        },
        {
            breakpoint: 767,
             settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: true,
                    arrows: false
                }
        }
    ]
  });

  // scheme section SVG vars
  var scheme_svg_1 = new Walkway({
    selector: '#dealers-scheme-svg-1',
    duration: '2000',
    // can pass in a function or a string like 'easeOutQuint'
    // easing: function (t) {
    //   return t * t;
    // }
  });

  var scheme_svg_2 = new Walkway({
    selector: '#dealers-scheme-svg-2',
    duration: '2000',
  });

  var scheme_svg_3 = new Walkway({
    selector: '#dealers-scheme-svg-3',
    duration: '2000',
  });

  var scheme_svg_4 = new Walkway({
    selector: '#dealers-scheme-svg-4',
    duration: '2000',
  });

  // faq section SVG vars
  let faq_svg_1 = new Walkway({
    selector: '#dealers-faq-svg-1',
    duration: '2000',
  });
  let faq_svg_2 = new Walkway({
    selector: '#dealers-faq-svg-2',
    duration: '2000',
  });
  let faq_svg_3 = new Walkway({
    selector: '#dealers-faq-svg-3',
    duration: '2000',
  });
  let faq_svg_4 = new Walkway({
    selector: '#dealers-faq-svg-4',
    duration: '2000',
  });
  let faq_svg_5 = new Walkway({
    selector: '#dealers-faq-svg-5',
    duration: '2000',
  });

  // cash SVG var
  let dealers_wyg_svg_1 = new Walkway({
    selector: '#dealers-wyg-svg-1',
    duration: '2000',
  });


// become a dealer SVG vars
  let bad_svg_1 = new Walkway({
    selector: '#dealers-become-a-dealer-svg-1',
    duration: '2000',
  });
  let bad_svg_2 = new Walkway({
    selector: '#dealers-become-a-dealer-svg-2',
    duration: '2000',
  });
  let bad_svg_3 = new Walkway({
    selector: '#dealers-become-a-dealer-svg-3',
    duration: '2000',
  });
  let bad_svg_4 = new Walkway({
    selector: '#dealers-become-a-dealer-svg-4',
    duration: '2000',
  });
  let bad_svg_5 = new Walkway({
    selector: '#dealers-become-a-dealer-svg-5',
    duration: '2000',
  });
  let bad_svg_6 = new Walkway({
    selector: '#dealers-become-a-dealer-svg-6',
    duration: '2000',
  });


///////////////////////////////////////////////////////
// dealers SCHEME svg animation
///////////////////////////////////////////////////////
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-scheme-svg-1'),
  handler: function(direction) {
    scheme_svg_1.redraw();
  },
  offset: 'bottom-in-view'
})
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-scheme-svg-1'),
  handler: function(direction) {
    scheme_svg_1.redraw();
  },
  })

  var waypoint = new Waypoint({
  element: document.getElementById('dealers-scheme-svg-2'),
  handler: function(direction) {
    scheme_svg_2.redraw();
  },
  offset: 'bottom-in-view'
})
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-scheme-svg-2'),
  handler: function(direction) {
    scheme_svg_2.redraw();
  },
  })
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-scheme-svg-3'),
  handler: function(direction) {
    scheme_svg_3.redraw();
  },
  offset: 'bottom-in-view'
})
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-scheme-svg-3'),
  handler: function(direction) {
    scheme_svg_3.redraw();
  },
  })
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-scheme-svg-4'),
  handler: function(direction) {
    scheme_svg_4.redraw();
  },
  offset: 'bottom-in-view'
})
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-scheme-svg-4'),
  handler: function(direction) {
    scheme_svg_4.redraw();
  },
  })

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
// dealers FAQ svg animation
///////////////////////////////////////////////////////
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-faq-svg-1'),
  handler: function(direction){
    faq_svg_1.redraw();
  },
  offset: 'bottom-in-view'
})
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-faq-svg-1'),
  handler: function(direction) {
    faq_svg_1.redraw();
  },
  })

  var waypoint = new Waypoint({
  element: document.getElementById('dealers-faq-svg-2'),
  handler: function(direction){
    faq_svg_2.redraw();

  },
  offset: 'bottom-in-view'
})
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-faq-svg-2'),
  handler: function(direction) {
    faq_svg_2.redraw();
  },
  })

  var waypoint = new Waypoint({
  element: document.getElementById('dealers-faq-svg-3'),
  handler: function(direction){
    faq_svg_3.redraw();
  },
  offset: 'bottom-in-view'
})
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-faq-svg-3'),
  handler: function(direction) {
    faq_svg_3.redraw();
  },
  })

  var waypoint = new Waypoint({
  element: document.getElementById('dealers-faq-svg-4'),
  handler: function(direction){
    faq_svg_4.redraw();
  },
  offset: 'bottom-in-view'
})
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-faq-svg-4'),
  handler: function(direction) {
    faq_svg_4.redraw();
  },
  })

  var waypoint = new Waypoint({
  element: document.getElementById('dealers-faq-svg-5'),
  handler: function(direction){
    faq_svg_5.redraw();
  },
  offset: 'bottom-in-view'
})
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-faq-svg-5'),
  handler: function(direction) {
    faq_svg_5.redraw();
  },
  })


  ///////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////
  // dealers WYG svg animation
  ///////////////////////////////////////////////////////
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-wyg-svg-1'),
  handler: function(direction) {
    dealers_wyg_svg_1.redraw();
  },
  offset: 'bottom-in-view'
})
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-wyg-svg-1'),
  handler: function(direction) {
    dealers_wyg_svg_1.redraw();
  },
  })

  ///////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////
  // dealers BECOME A DEALER svg animation
  ///////////////////////////////////////////////////////
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-become-a-dealer-svg-1'),
  handler: function(direction) {
    bad_svg_1.redraw();
  },
  offset: 'bottom-in-view'
})
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-become-a-dealer-svg-1'),
  handler: function(direction) {
    bad_svg_1.redraw();
  },
  })
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-become-a-dealer-svg-2'),
  handler: function(direction) {
    bad_svg_2.redraw();
  },
  offset: 'bottom-in-view'
})
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-become-a-dealer-svg-2'),
  handler: function(direction) {
    bad_svg_2.redraw();
  },
  })
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-become-a-dealer-svg-3'),
  handler: function(direction) {
    bad_svg_3.redraw();
  },
  offset: 'bottom-in-view'
})
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-become-a-dealer-svg-3'),
  handler: function(direction) {
    bad_svg_3.redraw();
  },
  })
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-become-a-dealer-svg-4'),
  handler: function(direction) {
    bad_svg_4.redraw();
  },
  offset: 'bottom-in-view'
})
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-become-a-dealer-svg-4'),
  handler: function(direction) {
    bad_svg_4.redraw();
  },
  })
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-become-a-dealer-svg-5'),
  handler: function(direction) {
    bad_svg_5.redraw();
  },
  offset: 'bottom-in-view'
})
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-become-a-dealer-svg-5'),
  handler: function(direction) {
    bad_svg_5.redraw();
  },
  })
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-become-a-dealer-svg-6'),
  handler: function(direction) {
    bad_svg_6.redraw();
  },
  offset: 'bottom-in-view'
})
  var waypoint = new Waypoint({
  element: document.getElementById('dealers-become-a-dealer-svg-6'),
  handler: function(direction) {
    bad_svg_6.redraw();
  },
  })
  ///////////////////////////////////



  function numberFormat(_number, _sep) {
  _number = Math.round(_number).toString();
  _number = typeof _number != "undefined" && _number > 0 ? _number : "";
  _number = _number.replace(new RegExp("^(\\d{" + (_number.length%3? _number.length%3:0) + "})(\\d{3})", "g"), "$1 $2").replace(/(\d{3})+?/gi, "$1 ").trim();
  if(typeof _sep != "undefined" && _sep != " ") {
      _number = _number.replace(/\s/g, _sep);
  }
  return _number;}



});
