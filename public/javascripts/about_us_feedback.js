$( document ).ready(function() {
  $('.js-main-feedback').slick({
    slidesToShow: 1,
    infinite: false,
    draggable: false,
    arrows: true,
    prevArrow: $('.main-feedback-arrows-left'),
    nextArrow: $('.main-feedback-arrows-right'),
    responsive: [
      {
        breakpoint: 1240,
        settings: {
          draggable: true,
          arrows: false,
          dots: true
        }
      }
    ]
  });

  $('.magnific-image').magnificPopup({
    type: 'image',
    mainClass: 'mfp-with-zoom',
    zoom: {
      enabled: true,
      duration: 300,
      easing: 'ease-in-out',
      opener: (el) => el.is('img') ? el : el.find('img')
    }
  });

  $('.magnific-feeback-button').magnificPopup({
    type: 'image',
    mainClass: 'mfp-with-zoom',
  });


});
