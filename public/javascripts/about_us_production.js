$( document ).ready(function() {
  ///////////////////////////////////////////////////////
  // about us > production svg declaration with walkway
  ///////////////////////////////////////////////////////
  var production_svg_1 = new Walkway({
    selector: '#production-svg-1',
    duration: '2000',
  });

  var production_svg_2 = new Walkway({
    selector: '#production-svg-2',
    duration: '2000',
  });

  var production_svg_3 = new Walkway({
    selector: '#production-svg-3',
    duration: '2000',
  });

  var production_svg_4 = new Walkway({
    selector: '#production-svg-4',
    duration: '2000',
  });

  var production_svg_5 = new Walkway({
    selector: '#production-svg-5',
    duration: '2000',
  });

  var production_svg_6 = new Walkway({
    selector: '#production-svg-6',
    duration: '2000',
  });

  var production_svg_7 = new Walkway({
    selector: '#production-svg-7',
    duration: '2000',
  });

  ///////////////////////////////////////////////////////
  // about us > production svg animation
  ///////////////////////////////////////////////////////
    var waypoint = new Waypoint({
    element: document.getElementById('production-svg-1'),
    handler: function(direction) {
      production_svg_1.redraw();
    },
    offset: 'bottom-in-view'
    })
    var waypoint = new Waypoint({
    element: document.getElementById('production-svg-1'),
    handler: function(direction) {
      production_svg_1.redraw();
    },
    })

    var waypoint = new Waypoint({
    element: document.getElementById('production-svg-2'),
    handler: function(direction) {
      production_svg_2.redraw();
    },
    offset: 'bottom-in-view'
    })
    var waypoint = new Waypoint({
    element: document.getElementById('production-svg-2'),
    handler: function(direction) {
      production_svg_2.redraw();
    },

    })
    var waypoint = new Waypoint({
    element: document.getElementById('production-svg-3'),
    handler: function(direction) {
      production_svg_3.redraw();
    },
    offset: 'bottom-in-view'
    })
    var waypoint = new Waypoint({
    element: document.getElementById('production-svg-3'),
    handler: function(direction) {
      production_svg_3.redraw();
    },
    })

    var waypoint = new Waypoint({
    element: document.getElementById('production-svg-4'),
    handler: function(direction) {
      production_svg_4.redraw();
    },
    offset: 'bottom-in-view'
    })
    var waypoint = new Waypoint({
    element: document.getElementById('production-svg-4'),
    handler: function(direction) {
      production_svg_4.redraw();
    },
    })

    var waypoint = new Waypoint({
    element: document.getElementById('production-svg-5'),
    handler: function(direction) {
      production_svg_5.redraw();
    },
    offset: 'bottom-in-view'
    })
    var waypoint = new Waypoint({
    element: document.getElementById('production-svg-5'),
    handler: function(direction) {
      production_svg_5.redraw();
    },
    })

    var waypoint = new Waypoint({
    element: document.getElementById('production-svg-6'),
    handler: function(direction) {
      production_svg_6.redraw();
    },
    offset: 'bottom-in-view'
    })
    var waypoint = new Waypoint({
    element: document.getElementById('production-svg-6'),
    handler: function(direction) {
      production_svg_6.redraw();
    },
    })

    var waypoint = new Waypoint({
    element: document.getElementById('production-svg-7'),
    handler: function(direction) {
      production_svg_7.redraw();
    },
    offset: 'bottom-in-view'
    })
    var waypoint = new Waypoint({
    element: document.getElementById('production-svg-7'),
    handler: function(direction) {
      production_svg_7.redraw();
    },
    })

  $(".production-process__description_specs_inner").on('click', function(){
    $(this).closest(".production-process__description").find(".production-process__description_dropdown").slideToggle();

    let arrowEl = $(this).find('.js-production-arrow');
    if (arrowEl.hasClass('js-production-arrow-state'))
    {
      arrowEl.removeClass('js-production-arrow-state');
      arrowEl.text('Открыть');
    } else {
      arrowEl.addClass('js-production-arrow-state');
      arrowEl.text('Закрыть');
    }

    let arrowElMobile = $(this);
    if (arrowElMobile.hasClass('js-production-arrow-mobile-state'))
    {
      arrowElMobile.removeClass('js-production-arrow-mobile-state');
    } else {
      arrowElMobile.addClass('js-production-arrow-mobile-state');
    }
  });
});
