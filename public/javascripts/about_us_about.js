$( document ).ready(function() {
  $('.js-main-feedback').slick({
    slidesToShow: 1,
    infinite: false,
    draggable: false,
    arrows: true,
    prevArrow: $('.main-feedback-arrows-left'),
    nextArrow: $('.main-feedback-arrows-right'),
    responsive: [
      {
        breakpoint: 1240,
        settings: {
          draggable: true,
          arrows: false,
          dots: true
        }
      }
    ]
  });

  $('.magnific-image').magnificPopup({
    type: 'image',
    mainClass: 'mfp-with-zoom',
    zoom: {
      enabled: true,
      duration: 300,
      easing: 'ease-in-out',
      opener: (el) => el.is('img') ? el : el.find('img')
    }
  });

  var about_us_svg_1 = new Walkway({
    selector: '#about-us-svg-1',
    duration: '2000',
  });

  var about_us_svg_2 = new Walkway({
    selector: '#about-us-svg-2',
    duration: '2000',
  });

  var about_us_svg_3 = new Walkway({
    selector: '#about-us-svg-3',
    duration: '2000',
  });

  var about_us_svg_4 = new Walkway({
    selector: '#about-us-svg-4',
    duration: '2000',
  });

  var about_us_svg_5 = new Walkway({
    selector: '#about-us-svg-5',
    duration: '2000',
  });

  var about_us_svg_6 = new Walkway({
    selector: '#about-us-svg-6',
    duration: '2000',
  });

  var about_us_svg_7 = new Walkway({
    selector: '#about-us-svg-7',
    duration: '2000',
  });

  ///////////////////////////////////////////////////////
  // about us svg animation
  ///////////////////////////////////////////////////////
  var waypoint = new Waypoint({
  element: document.getElementById('about-us-svg-1'),
  handler: function(direction) {
    about_us_svg_1.redraw();
  },
  offset: 'bottom-in-view'
  })
  var waypoint = new Waypoint({
  element: document.getElementById('about-us-svg-1'),
  handler: function(direction) {
    about_us_svg_1.redraw();
  },
  })

  var waypoint = new Waypoint({
  element: document.getElementById('about-us-svg-2'),
  handler: function(direction) {
    about_us_svg_2.redraw();
  },
  offset: 'bottom-in-view'
  })
  var waypoint = new Waypoint({
  element: document.getElementById('about-us-svg-2'),
  handler: function(direction) {
    about_us_svg_2.redraw();
  },
  })

  var waypoint = new Waypoint({
  element: document.getElementById('about-us-svg-3'),
  handler: function(direction) {
    about_us_svg_3.redraw();
  },
  offset: 'bottom-in-view'
  })
  var waypoint = new Waypoint({
  element: document.getElementById('about-us-svg-3'),
  handler: function(direction) {
    about_us_svg_3.redraw();
  },
  })

  var waypoint = new Waypoint({
  element: document.getElementById('about-us-svg-4'),
  handler: function(direction) {
    about_us_svg_4.redraw();
  },
  offset: 'bottom-in-view'
  })
  var waypoint = new Waypoint({
  element: document.getElementById('about-us-svg-4'),
  handler: function(direction) {
    about_us_svg_4.redraw();
  },
  })

  var waypoint = new Waypoint({
  element: document.getElementById('about-us-svg-5'),
  handler: function(direction) {
    about_us_svg_5.redraw();
  },
  offset: 'bottom-in-view'
  })
  var waypoint = new Waypoint({
  element: document.getElementById('about-us-svg-5'),
  handler: function(direction) {
    about_us_svg_5.redraw();
  },
  })

  var waypoint = new Waypoint({
  element: document.getElementById('about-us-svg-6'),
  handler: function(direction) {
    about_us_svg_6.redraw();
  },
  offset: 'bottom-in-view'
  })
  var waypoint = new Waypoint({
  element: document.getElementById('about-us-svg-6'),
  handler: function(direction) {
    about_us_svg_6.redraw();
  },
  })

  var waypoint = new Waypoint({
  element: document.getElementById('about-us-svg-7'),
  handler: function(direction) {
    about_us_svg_7.redraw();
  },
  offset: 'bottom-in-view'
  })
  var waypoint = new Waypoint({
  element: document.getElementById('about-us-svg-7'),
  handler: function(direction) {
    about_us_svg_7.redraw();
  },
  })



});
