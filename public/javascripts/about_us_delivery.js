$( document ).ready(function() {
  ///////////////////////////////////////////////////////
  // about us > delivery > landing svg declaration with walkway
  ///////////////////////////////////////////////////////
  var delivery_landing_svg_1 = new Walkway({
    selector: '#delivery-landing-svg-1',
    duration: '2000',
  });

  var delivery_landing_svg_2 = new Walkway({
    selector: '#delivery-landing-svg-2',
    duration: '2000',
  });

  var delivery_landing_svg_3 = new Walkway({
    selector: '#delivery-landing-svg-3',
    duration: '2000',
  });

  var delivery_landing_svg_4 = new Walkway({
    selector: '#delivery-landing-svg-4',
    duration: '2000',
  });

  var delivery_landing_svg_5 = new Walkway({
    selector: '#delivery-landing-svg-5',
    duration: '2000',
  });

  var delivery_landing_svg_6 = new Walkway({
    selector: '#delivery-landing-svg-6',
    duration: '2000',
  });

  var delivery_landing_svg_7 = new Walkway({
    selector: '#delivery-landing-svg-7',
    duration: '2000',
  });

  ///////////////////////////////////////////////////////
  // about us > delivery > landing svg animation
  ///////////////////////////////////////////////////////
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-landing-svg-1'),
  handler: function(direction) {
    delivery_landing_svg_1.redraw();},
  offset: 'bottom-in-view' })
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-landing-svg-1'),
  handler: function(direction) {
    delivery_landing_svg_1.redraw(); }, })

  var waypoint = new Waypoint({
  element: document.getElementById('delivery-landing-svg-2'),
  handler: function(direction) {
    delivery_landing_svg_2.redraw(); },
  offset: 'bottom-in-view' })
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-landing-svg-2'),
  handler: function(direction) {
    delivery_landing_svg_2.redraw(); }, })

  var waypoint = new Waypoint({
  element: document.getElementById('delivery-landing-svg-3'),
  handler: function(direction) {
    delivery_landing_svg_3.redraw(); },
  offset: 'bottom-in-view' })
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-landing-svg-3'),
  handler: function(direction) {
    delivery_landing_svg_3.redraw(); }, })

  var waypoint = new Waypoint({
  element: document.getElementById('delivery-landing-svg-4'),
  handler: function(direction) {
    delivery_landing_svg_4.redraw(); },
  offset: 'bottom-in-view' })
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-landing-svg-4'),
  handler: function(direction) {
    delivery_landing_svg_4.redraw(); }, })

  var waypoint = new Waypoint({
  element: document.getElementById('delivery-landing-svg-5'),
  handler: function(direction) {
    delivery_landing_svg_5.redraw(); },
  offset: 'bottom-in-view' })
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-landing-svg-5'),
  handler: function(direction) {
    delivery_landing_svg_5.redraw(); }, })

  var waypoint = new Waypoint({
  element: document.getElementById('delivery-landing-svg-6'),
  handler: function(direction) {
    delivery_landing_svg_6.redraw(); },
  offset: 'bottom-in-view' })
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-landing-svg-6'),
  handler: function(direction) {
    delivery_landing_svg_6.redraw(); }, })

  var waypoint = new Waypoint({
  element: document.getElementById('delivery-landing-svg-7'),
  handler: function(direction) {
    delivery_landing_svg_7.redraw(); },
  offset: 'bottom-in-view' })
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-landing-svg-7'),
  handler: function(direction) {
    delivery_landing_svg_7.redraw(); }, })


  ///////////////////////////////////////////////////////
  // about us > delivery > types svg declaration with walkway
  ///////////////////////////////////////////////////////
  var delivery_types_svg_1 = new Walkway({
    selector: '#delivery-types-svg-1',
    duration: '2000',
  });

  var delivery_types_svg_2 = new Walkway({
    selector: '#delivery-types-svg-2',
    duration: '2000',
  });

  var delivery_types_svg_3 = new Walkway({
    selector: '#delivery-types-svg-3',
    duration: '2000',
  });

  var delivery_types_svg_4 = new Walkway({
    selector: '#delivery-types-svg-4',
    duration: '2000',
  });

  ///////////////////////////////////////////////////////
  // about us > delivery > types svg animation
  ///////////////////////////////////////////////////////
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-types-svg-1'),
  handler: function(direction) {
    delivery_types_svg_1.redraw();},
  offset: 'bottom-in-view' })
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-types-svg-1'),
  handler: function(direction) {
    delivery_types_svg_1.redraw(); }, })

  var waypoint = new Waypoint({
  element: document.getElementById('delivery-types-svg-2'),
  handler: function(direction) {
    delivery_types_svg_2.redraw();},
  offset: 'bottom-in-view' })
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-types-svg-2'),
  handler: function(direction) {
    delivery_types_svg_2.redraw(); }, })

  var waypoint = new Waypoint({
  element: document.getElementById('delivery-types-svg-3'),
  handler: function(direction) {
    delivery_types_svg_3.redraw();},
  offset: 'bottom-in-view' })
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-types-svg-3'),
  handler: function(direction) {
    delivery_types_svg_3.redraw(); }, })

  var waypoint = new Waypoint({
  element: document.getElementById('delivery-types-svg-4'),
  handler: function(direction) {
    delivery_types_svg_4.redraw();},
  offset: 'bottom-in-view' })
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-types-svg-4'),
  handler: function(direction) {
    delivery_types_svg_4.redraw(); }, })

  ///////////////////////////////////////////////////////
  // about us > delivery > price svg declaration with walkway
  ///////////////////////////////////////////////////////
  var delivery_price_svg_1 = new Walkway({
    selector: '#delivery-price-svg-1',
    duration: '2000',
  });

  var delivery_price_svg_2 = new Walkway({
    selector: '#delivery-price-svg-2',
    duration: '2000',
  });

  var delivery_price_svg_3 = new Walkway({
    selector: '#delivery-price-svg-3',
    duration: '2000',
  });

  ///////////////////////////////////////////////////////
  // about us > delivery > price svg animation
  ///////////////////////////////////////////////////////
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-price-svg-1'),
  handler: function(direction) {
    delivery_price_svg_1.redraw();},
  offset: 'bottom-in-view' })
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-price-svg-1'),
  handler: function(direction) {
    delivery_price_svg_1.redraw(); }, })

  var waypoint = new Waypoint({
  element: document.getElementById('delivery-price-svg-2'),
  handler: function(direction) {
    delivery_price_svg_2.redraw();},
  offset: 'bottom-in-view' })
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-price-svg-2'),
  handler: function(direction) {
    delivery_price_svg_2.redraw(); }, })

  var waypoint = new Waypoint({
  element: document.getElementById('delivery-price-svg-3'),
  handler: function(direction) {
    delivery_price_svg_3.redraw();},
  offset: 'bottom-in-view' })
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-price-svg-3'),
  handler: function(direction) {
    delivery_price_svg_3.redraw(); }, })

  ///////////////////////////////////////////////////////
  // about us > delivery > payment svg declaration with walkway
  ///////////////////////////////////////////////////////
  var delivery_payment_svg = new Walkway({
    selector: '#delivery-payment-svg',
    duration: '2000',
  });

  var waypoint = new Waypoint({
  element: document.getElementById('delivery-payment-svg'),
  handler: function(direction) {
    delivery_payment_svg.redraw();},
  offset: 'bottom-in-view' })
  var waypoint = new Waypoint({
  element: document.getElementById('delivery-payment-svg'),
  handler: function(direction) {
    delivery_payment_svg.redraw(); }, })
});
