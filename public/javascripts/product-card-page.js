$( document ).ready(function() {

    $('#tabs').tabs();

    $('.magnific-image').magnificPopup({type:'image'});


    $(".pc-diam>li").on('click', function(event){
      event.preventDefault();
      $('.pc-diam>li').each(function(){
        $(this).removeClass('pc-selector-active');
      });
      $(this).addClass('pc-selector-active');
      var text = $(this).data('value');
      $('.pc-med-selected-diam').text(text);
      var id = $(this).data('diameter');
      $('.pc-med-selected-diam').attr('id', id);

      $('.pc-caps-collapse, .pc-caps').addClass('nonshown');
      $('#' + id + 'cap').removeClass('nonshown');
      $('#' + id + 'steel').addClass('pc-selector-active');
      $('#' + id + 'plastik[data-cap="plastik"]').removeClass('pc-selector-active');

      $('.pc-prices-price').removeClass('pc-active');
      $('#pc-new-' + id + 'steel').addClass('pc-active');
      $('.pc-prices-old-price').removeClass('pc-active');
      $('#pc-old-' + id).addClass('pc-active');
      $('.pc-prices-discount').removeClass('pc-active');
      $('#' + id + 'discount').addClass('pc-active');
      $('.pc-add-to-cart.pc-add-to-cart-item').removeClass('already-in-cart');

      // clear primer selector
      $('.pc-primer>li:not(:first-child)').each(function(){
        $(this).removeClass('pc-selector-active');
      });
      $('.pc-primer>li:last-child').addClass('pc-selector-active');
    });

    $(".pc-caps>li").on('click', function(event){
      event.preventDefault();
      $('.pc-caps>li').each(function(){
        $(this).removeClass('pc-selector-active');
      });
      $(this).addClass('pc-selector-active');
      var text = $(this).data('value');
      $('.pc-med-selected-caps').text(text);

      let id = $('.pc-med-selected-diam').attr('id');
      console.log(id);
      let cap = $(this).attr('data-cap');
      console.log(cap);
      $('.pc-prices-price').removeClass('pc-active');
      $('#pc-new-' + id + cap).addClass('pc-active');

      // clear primer selector
      $('.pc-primer>li:not(:first-child)').each(function(){
        $(this).removeClass('pc-selector-active');
      });
      $('.pc-primer>li:last-child').addClass('pc-selector-active');
    });

    $(".pc-primer>li:not(:first-child)").on('click', function(event){
      event.preventDefault();
      if(!$(this).hasClass('pc-selector-active')){
        $('.pc-primer>li:not(:first-child)').each(function(){
          $(this).removeClass('pc-selector-active');
        });
        $(this).addClass('pc-selector-active');
        var text = $(this).data('value');
        let id = $('.pc-med-selected-diam').attr('id');
        let cap = $('ul#' + id + 'cap li.pc-selector-active').attr('data-cap');
        if(typeof cap === 'undefined') {
          cap = 'steel';
        }

        $('#pc-new-' + id + cap).addClass('pc-active');
        if(text == 'Да') {
          $('.pc-prices-price').removeClass('pc-active');
          $('#pc-new-' + id + cap + '-primer').addClass('pc-active');
        }
        else {
          $('.pc-prices-price').removeClass('pc-active');
          $('#pc-new-' + id + cap).addClass('pc-active');
        }
      }


      // $('.pc-med-selected-caps').text(text);

      // let id = $('.pc-med-selected-diam').attr('id');
      // console.log(id);
      // let cap = $(this).attr('data-cap');
      // console.log(cap);
      // $('.pc-prices-price').removeClass('pc-active');
      // $('#pc-new-' + id + cap).addClass('pc-active');
    });

    $(".pc-strap-color>li").on('click', 'a', function(event){
      event.preventDefault();
      $('.pc-strap-color>li a').each(function(){
        $(this).removeClass('active');
      });
      $(this).addClass('active');
    });


    $(".pc-rack-color>li").on('click', 'a', function(event){
      event.preventDefault();
      $('.pc-rack-color>li a').each(function(){
        $(this).removeClass('active');
      });
      $(this).addClass('active');
    });

    $(".pc-diam-collapse").on('click', function(){
      $('.pc-diam').slideToggle();
      var arrow = $(this).find('.pc-collapse-arrow')
      if (arrow.hasClass('arrow_rotate')) {
        arrow.removeClass('arrow_rotate');
      } else {
        arrow.addClass('arrow_rotate');
      };
    });

    $(".pc-caps-collapse").on('click', function(){
      $('.pc-caps').slideToggle();
      var arrow = $(this).find('.pc-collapse-arrow')
      if (arrow.hasClass('arrow_rotate')) {
        arrow.removeClass('arrow_rotate');
      } else {
        arrow.addClass('arrow_rotate');
      };
    });

    $('.pc-info-tabs-mobile-title').on('click', function(){
      $(this).next().slideToggle();
      var arrow = $(this).find('.pc-collapse-arrow')
      if (arrow.hasClass('arrow_rotate')) {
        arrow.removeClass('arrow_rotate');
      } else {
        arrow.addClass('arrow_rotate');
      };
    });

    //custom select
    $('.pc-color-picker-js select').each(function(index){
      var $this = $(this), numberOfOptions = $(this).children('option').length;
      var index1 = index + 1;
      $this.addClass('select-hidden');
      $this.wrap('<div class="select"></div>');
      $this.after('<div class="select-styled pc-config-color" data-colorgroup="' + index1 + '"></div>');

      var $styledSelect = $this.next('div.select-styled');
      $styledSelect.html('<span class="dgrey" data-color="1"></span>');
      if(index1 == 1){
        $styledSelect.html('<span class="orange" data-color="5"></span>');
      }

      var $list = $('<ul />', {
        'class': 'color-select-options'
      }).insertAfter($styledSelect);

      for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
          html: '<span></span>',
          rel: $this.children('option').eq(i).data('color'),
          class: $this.children('option').eq(i).val()
        }).appendTo($list);
      }

      var $listItems = $list.children('li');

      $styledSelect.click(function(e) {
        e.stopPropagation();

        $('div.select-styled.active').not(this).each(function(){
          $(this).removeClass('active').next('ul.color-select-options').hide();
        });

        $(this).toggleClass('active').next('ul.color-select-options').toggle();
      });

      $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.html(`<span class=${$(this).attr('class')} data-color=${$(this).attr('rel')}></span>`).removeClass('active');
        $list.hide();
      });

      $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
      });
    });

    // document.querySelectorAll('.clamps-table-collapse').forEach((root) => {
    // root.querySelector('.clamps-table-collapse-button').addEventListener('click', () => {
    //   if (root.classList.contains('opened')) {
    //     $(root.querySelector('.clamps-table-collapse-content')).slideUp(150); // jquery
    //     root.classList.remove('opened');
    //   } else {
    //     $(root.querySelector('.clamps-table-collapse-content')).slideDown(150); // jquery
    //     root.classList.add('opened');
    //   }
    // });
    // });



/* КАРТОЧКА ТОВАРА - КНОПКА "В КОРЗИНУ" */
  body.on('click','.js-pc-prices-add-to-cart', function(event){
      event.preventDefault();
      //if ($(this).hasClass('already-in-cart')) {
      //    return false;
      //}
      //$(this).addClass('already-in-cart');
      var e = $(this).parents('.pc-wrapper-main-content');
      art = e.find('.pc-main-subtitle .value').text();
      diameter = e.find('.pc-diam .pc-selector-active').data('diameter');
      article = '';
      if(!$('.pc-caps').hasClass('.nonshown')){
        var cap = $('#' + diameter + 'cap li.pc-selector-active').data('cap');
        article = art + '-' + diameter + '-' + cap;
      } else {
        article = art + '-' + diameter + '-steel';
      }
      if($('.pc-primer>li:nth-child(2)').hasClass('pc-selector-active')) {
        article = article + '-primer';
      }

      addToCart(article,1);
      var cart = getCart();


      cart[article].diameter = diameter;
      cart[article].key = article;
      cart[article].primer = $('.pc-primer>li.pc-selector-active').data('value');

      cart[article].article = art;
      if(cap){
        cart[article].cap = caps[cap];
      }
      else {
        cart[article].cap = caps['steel'];
      }

      cart[article].img = e.find('.pc-pic-slider-main-pic img').attr('src');
      cart[article].title = e.find('.pc-main-title').text();

      cart[article].price_current = e.find('.pc-prices-price.pc-active span').text().replace(/ /g,'');
      cart[article].price_old = e.find('.pc-prices-old-price.pc-active span').text().replace(/ /g,'');
      cart[article].price_discount = parseInt(cart[article].price_current) * 0.05;

      e.find('.pc-config-color').each(function(){
          var colorGroup = $(this).data('colorgroup');
          cart[article]['color' + colorGroup] = parseInt($(this).children('span').data('color'))||1;
      });
      setCart(cart);
      calculateCart();
      //alert('Товар добавлен в корзину');
      $('.js-added-to-cart-popup').click();
      $( '#item' ).parent().scrollTop(10000);
      // return false;
  });


/* КАРТОЧКА ТОВАРА - КНОПКА "КУПИТЬ" */
  body.on('click','.js-pc-prices-buy', function(event){
      event.preventDefault();
      // if ($(this).hasClass('already-in-cart')) {
      //     return false;
      // }
      // $(this).addClass('already-in-cart');
      var e = $(this).parents('.pc-wrapper-main-content');
      art = e.find('.pc-main-subtitle .value').text();
      diameter = e.find('.pc-diam .pc-selector-active').data('diameter');
      article = '';
      if(!$('.pc-caps').hasClass('.nonshown')){
        var cap = $('#' + diameter + 'cap li.pc-selector-active').data('cap');
        article = art + '-' + diameter + '-' + cap;
      } else {
        article = art + '-' + diameter + '-steel';
      }
      if($('.pc-primer>li:nth-child(2)').hasClass('pc-selector-active')) {
        article = article + '-primer';
      }

      addToCart(article,1);
      var cart = getCart();

      cart[article].diameter = diameter;
      cart[article].key = article;
      cart[article].primer = $('.pc-primer>li.pc-selector-active').data('value');

      cart[article].article = art;
      if(cap){
        cart[article].cap = caps[cap];
      }
      else {
        cart[article].cap = caps['steel'];
      }

      cart[article].img = e.find('.pc-pic-slider-main-pic img').attr('src');
      cart[article].title = e.find('.pc-main-title').text();

      cart[article].price_current = e.find('.pc-prices-price.pc-active span').text().replace(/ /g,'');
      cart[article].price_old = e.find('.pc-prices-old-price.pc-active span').text().replace(/ /g,'');
      cart[article].price_discount = parseInt(cart[article].price_current) * 0.05;

      e.find('.pc-config-color').each(function(){
          var colorGroup = $(this).data('colorgroup');
          cart[article]['color' + colorGroup] = parseInt($(this).children('span').data('color'))||1;
      });
      setCart(cart);
      calculateCart();
      window.location = '/cart#cart_step_2';
      // return false;
  });
});
