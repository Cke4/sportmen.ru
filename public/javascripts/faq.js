$( document ).ready(function() {



  $( "#faq-accordion" ).accordion({
    active: false,
    header: ".faq-acc-header",
    heightStyle: "content",
    collapsible: true,
    create: function( event, ui ) {
      console.log(ui);
      $(ui.header).find('.faq-title-arrow').addClass('arrow_rotate');
    },
    activate: function( event, ui ) {
      $(ui.newHeader).find('.faq-title-arrow').addClass('arrow_rotate');
      $(ui.oldHeader).find('.faq-title-arrow').removeClass('arrow_rotate');
      let accActivated = $(ui.newPanel).find('.faq-accordion-content__slider').data('initiated');
      // reinitialization of slick bugs out so I need to check if slider in this particular accordion tab has already been initiated or not
      if (accActivated == 0) {
        $(ui.newPanel).find('.faq-accordion-content__slider').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: false,
          autoplaySpeed: 2000,
          responsive: [
              {
                  breakpoint: 9999,
                  settings: "unslick"
              },
              {
                  breakpoint: 767,
                   settings: {
                          slidesToShow: 1,
                          slidesToScroll: 1,
                          infinite: false,
                          dots: true,
                          arrows: false
                      }
              }
          ]
        });
        $(ui.newPanel).find('.faq-accordion-content__slider').data('initiated', 1);
      }
    },
    classes: {
      "ui-accordion": "faq-accordion",
      "ui-accordion-header": "faq-accordion-header",
      "ui-accordion-header-active": "faq-accordion-header-active",
      "ui-accordion-content": "faq-accordion-content",
      "ui-accordion-content-active": "faq-accordion-content-active",
    }
  });

  // $('.faq-accordion-content__slider').slick({
  //   slidesToShow: 1,
  //   slidesToScroll: 1,
  //   autoplay: false,
  //   autoplaySpeed: 2000,
  //   responsive: [
  //       {
  //           breakpoint: 9999,
  //           settings: "unslick"
  //       },
  //       {
  //           breakpoint: 767,
  //            settings: {
  //                   slidesToShow: 1,
  //                   slidesToScroll: 1,
  //                   infinite: false,
  //                   dots: true,
  //                   arrows: false
  //               }
  //       }
  //   ]
  // });

  $('.magnific-image').magnificPopup({type:'image'});

});
