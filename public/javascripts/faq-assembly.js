$(document).ready(function() {

  // first section SVG init
  let faq_assembly_svg_1 = new Walkway({
    selector: '#faq-assembly-requirements-1',
    duration: '2000',
  });

  let faq_assembly_svg_2 = new Walkway({
    selector: '#faq-assembly-requirements-2',
    duration: '2000',
  });

  let faq_assembly_svg_3 = new Walkway({
    selector: '#faq-assembly-requirements-3',
    duration: '2000',
  });

  let faq_assembly_svg_4 = new Walkway({
    selector: '#faq-assembly-requirements-4',
    duration: '2000',
  });


  // first section SVG animation
  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-requirements-1'),
  handler: function(direction) {
    faq_assembly_svg_1.redraw();
  },
  offset: 'bottom-in-view'
  });
  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-requirements-1'),
  handler: function(direction) {
    faq_assembly_svg_1.redraw();
  },
  });

  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-requirements-2'),
  handler: function(direction) {
    faq_assembly_svg_2.redraw();
  },
  offset: 'bottom-in-view'
  });
  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-requirements-2'),
  handler: function(direction) {
    faq_assembly_svg_2.redraw();
  },
  });

  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-requirements-3'),
  handler: function(direction) {
    faq_assembly_svg_3.redraw();
  },
  offset: 'bottom-in-view'
  });
  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-requirements-3'),
  handler: function(direction) {
    faq_assembly_svg_3.redraw();
  },
  });

  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-requirements-4'),
  handler: function(direction) {
    faq_assembly_svg_4.redraw();
  },
  offset: 'bottom-in-view'
  });
  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-requirements-4'),
  handler: function(direction) {
    faq_assembly_svg_4.redraw();
  },
  });

  //------------------------------------
  // assembly section SVG init
  //------------------------------------

  let faq_steps_svg_1 = new Walkway({
    selector: '#faq-assembly-steps-1',
    duration: '2000',
  });
  let faq_steps_svg_2 = new Walkway({
    selector: '#faq-assembly-steps-2',
    duration: '2000',
  });
  let faq_steps_svg_3 = new Walkway({
    selector: '#faq-assembly-steps-3',
    duration: '2000',
  });
  let faq_steps_svg_4 = new Walkway({
    selector: '#faq-assembly-steps-4',
    duration: '2000',
  });
  let faq_steps_svg_5 = new Walkway({
    selector: '#faq-assembly-steps-5',
    duration: '2000',
  });
  let faq_steps_svg_6 = new Walkway({
    selector: '#faq-assembly-steps-6',
    duration: '2000',
  });
  let faq_steps_svg_7 = new Walkway({
    selector: '#faq-assembly-steps-7',
    duration: '2000',
  });
  let faq_steps_svg_8 = new Walkway({
    selector: '#faq-assembly-steps-8',
    duration: '2000',
  });
  let faq_steps_svg_9 = new Walkway({
    selector: '#faq-assembly-steps-9',
    duration: '2000',
  });
  let faq_steps_svg_10 = new Walkway({
    selector: '#faq-assembly-steps-10',
    duration: '2000',
  });

  // assembly section SVG animation
  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-1'),
  handler: function(direction) {
    faq_steps_svg_1.redraw();
  },
  offset: 'bottom-in-view'
  });
  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-1'),
  handler: function(direction) {
    faq_steps_svg_1.redraw();
  },
  });

  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-2'),
  handler: function(direction) {
    faq_steps_svg_2.redraw();
  },
  offset: 'bottom-in-view'
  });
  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-2'),
  handler: function(direction) {
    faq_steps_svg_2.redraw();
  },
  });

  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-3'),
  handler: function(direction) {
    faq_steps_svg_3.redraw();
  },
  offset: 'bottom-in-view'
  });
  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-3'),
  handler: function(direction) {
    faq_steps_svg_3.redraw();
  },
  });

  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-4'),
  handler: function(direction) {
    faq_steps_svg_4.redraw();
  },
  offset: 'bottom-in-view'
  });
  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-4'),
  handler: function(direction) {
    faq_steps_svg_4.redraw();
  },
  });

  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-5'),
  handler: function(direction) {
    faq_steps_svg_5.redraw();
  },
  offset: 'bottom-in-view'
  });
  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-5'),
  handler: function(direction) {
    faq_steps_svg_5.redraw();
  },
  });

  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-6'),
  handler: function(direction) {
    faq_steps_svg_6.redraw();
  },
  offset: 'bottom-in-view'
  });
  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-6'),
  handler: function(direction) {
    faq_steps_svg_6.redraw();
  },
  });

  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-7'),
  handler: function(direction) {
    faq_steps_svg_7.redraw();
  },
  offset: 'bottom-in-view'
  });
  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-7'),
  handler: function(direction) {
    faq_steps_svg_7.redraw();
  },
  });

  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-8'),
  handler: function(direction) {
    faq_steps_svg_8.redraw();
  },
  offset: 'bottom-in-view'
  });
  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-8'),
  handler: function(direction) {
    faq_steps_svg_8.redraw();
  },
  });

  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-9'),
  handler: function(direction) {
    faq_steps_svg_9.redraw();
  },
  offset: 'bottom-in-view'
  });
  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-9'),
  handler: function(direction) {
    faq_steps_svg_9.redraw();
  },
  });

  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-10'),
  handler: function(direction) {
    faq_steps_svg_10.redraw();
  },
  offset: 'bottom-in-view'
  });
  var waypoint = new Waypoint({
  element: document.getElementById('faq-assembly-steps-10'),
  handler: function(direction) {
    faq_steps_svg_10.redraw();
  },
  });
});
