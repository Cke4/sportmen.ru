$( document ).ready(function() {
  
  //initializing jQUI tabs
  $('#cart_tabs').tabs({
    active: 0,
    disabled: [ 1 ]
  });  
  
  var hash = window.location.hash;
  if(hash == '#cart_step_2'){
    $("#cart_tabs").tabs( "enable" );
    $('#cart_tabs').tabs("option", "active", 1);
    $('#cart_page_menu_button_2').addClass('active');
  }

  $('.js-cart_submit_order').click(function(e){
    console.log(8888);
    e.preventDefault;
    var form = $('#card_form');
    var valid = jsValidCardForm();
    if(valid == true){
      var cart = getCart();
      var strResult = "";
        if($(this).hasClass('credit-btn')) {
          $('input[name="content[pay_type]"]').val("card");
        }      
      for (i in cart) {
        let diameter = '';
        if(cart[i].diameter){
          diameter = cart[i].diameter;
        }
        if(cart[i].cap){
          cap = cart[i].cap;
        }
        primer = cart[i].primer;
          if (cart[i] && cart[i]['count']) {
              strResult += i + ' : ' + cart[i]['count'] + 'шт, Хомуты: ' + colors[cart[i].color1] + ' цвет'
              + ', Стойки: ' + colors[cart[i].color2] + ' цвет, диаметр ' + diameter  + ', заглушки ' + cap  + ', грунтовка: ' + primer + '; <br />  \r\n';
         }
      }
      $('input[name="content[items]"]').val(strResult);
      var total = parseInt($('.cart_total_price_disc span').html().replace(/\s+/g, ''));
      $('input[name="content[total]"]').val(total);
      form.submit();
    } else {
      return false;
    }

  });

 

  var fileCheckbox = $('#cart_submit_card__checkbox');
  var fileInput = $('input[name="content[requ]"]');

  fileInput.change(function(){
      fileCheckbox.prop('checked', !!$(this).val());
  })

  fileCheckbox.change(function(){
      if (!$(this).prop('checked'))  {
          fileInput.val('');
      }
      else {
          fileInput.click();
      }
  });

  var global_price = 0;
  var global_discount = 0;


  $(".cart_file_upload").dropzone({ url: "/file/post" });
  $(".cart_file_upload p").dropzone({ url: "/file/post" });

  //switching to tab 0 back on mobile
  $('#cart_step_2_back_button').on('click', function(e){
    $('#cart_tabs').tabs("option", "active", 0);
    //$('#cart_page_menu_button_1').trigger('click');
    //$('#cart_page_menu_button_1')[0].click();
    // console.log($('#cart_page_menu_button_1'));
    // console.log('11111');
  });


  // cart tab switching
  // switch via "process order"
  $('.cart_s1pb_proceed').on('click', function(e){
    e.preventDefault();
    // console.log($('.cart_s1_product_list').children);
    // console.log($('.cart_s1_product_list').children().length);
    // $('.cart_s1_product_list').children().each(function(){
    //   console.log($(this));
    // });
    var added_products = $('.cart_s1_product_list .cart_added_product').length;
    if (added_products > 0) {
      $("#cart_tabs").tabs( "enable" );
      $('#cart_tabs').tabs("option", "active", 1);
      $('#cart_page_menu_button_2').addClass('active');
    } else {
      var ap_warning = $('.cart_s1_product_list .cart_empty_warning').length;
      if (ap_warning > 0) {
        //$('.cart_empty_warning').
      } else {
        basketIsEmpty();
      };
    };
  });
  // switching via tab button
  $('#cart_page_menu_button_2').on('click', function(){
    var added_products = $('.cart_s1_product_list .cart_added_product').length;
    if (added_products > 0) {
      $("#cart_tabs").tabs( "enable" );
      $('#cart_tabs').tabs("option", "active", 1);
      $(this).addClass('active');
    } else {
      basketIsEmpty();
    };
  });

// deactivating second button 'active' css on desktop if user clicks first tab
  $('#cart_page_menu_button_1').on('click', function(){
    $('#cart_page_menu_button_2').removeClass('active');
  });


// removing product from the cart
  $('.cart_s1_product_list').on('click', '.cart_ap_remove_button', function(){
    var target = $(this).closest('.cart_added_product');
    var article = target.data('id');
    //target.fadeOut(300, function(){ target.remove();});
    target.remove();
    removeFromCart(article, '');
    basketIsEmpty();
    countPrices();
  });



// populating the product list from the LS
var ls_cart_items = JSON.parse(localStorage.getItem('cart'));
var target = $('.cart_s1_product_list')
$.each(ls_cart_items, function(index, value){
  var article = ls_cart_items[index].article;
  var title = ls_cart_items[index].title;
  var count = ls_cart_items[index].count;
  var pic = ls_cart_items[index].img;
  var price = ls_cart_items[index].price_current;
  var price_old = ls_cart_items[index].price_old;
  var key = ls_cart_items[index].key;
  
  var discount_html = '';
  var price_old_html = '';
  if(price_old > 0){
  	var discount = Math.round(ls_cart_items[index].price_current * 100 / ls_cart_items[index].price_old - 100);
  	global_price += parseInt(price);
    discount_html = `<span class="cart_aptp_discount">${discount}%</span>`;
    price_old_html = `<span class="cart_aptp_old_price" data-old-price=${price_old*count}> ${numberFormat((price_old*count).toString(), ' ')} Р.</span>`;
  } 

  $(target).append(
    `
    <div class="cart_added_product" data-id='${key}'>
      <div class="cart_ap_mobile_text">
        <div class="cart_apm_article">АРТ.${article}</div>
        <div class="cart_apm_name">${title}</div>
      </div>
      <img src="${pic}" alt="" class="cart_ap_img">
      <div class="cart_ap_text">
        <span class="cart_apt_name">${title}</span>
        <span class="cart_apt_article">АРТ.${article}</span>
        <div class="cart_apt_prices">
          ${discount_html}
  		  ${price_old_html}
          <span class="cart_aptp_price" data-price=${price*count}>${numberFormat((price*count).toString(), ' ')} р.</span>
        </div>
      </div>
      <div class="cart_ap_buttons" data-count='${count}'>
        <div class="cart_apb_minus">–</div>
        <div class="cart_apb_amount">${count}шт</div>
        <div class="cart_apb_plus">+</div>
      </div>
      <div class="cart_ap_remove_button"></div>
    </div>
    `
  );
  countPrices();
});

basketIsEmpty();


// + and - buttons
$('.cart_s1_product_list').on('click', '.cart_apb_minus', function(){
  basketIsEmpty();
  var ls_cart_items = JSON.parse(localStorage.getItem('cart'));
  var id = $(this).closest('.cart_added_product').data('id');
  var count = ls_cart_items[id].count;
  var price_current = ls_cart_items[id].price_current;
  var price_old = ls_cart_items[id].price_old;
  count -= 1;
  if (count < 1) {
    $(this).closest('.cart_added_product').remove();
    removeItemFromWrapper(id);
    basketIsEmpty();
    removeFromCart(id);
  } else {
    var price = count * price_current;
    var price_o = count * price_old;
    var button_block = $(this).closest('.cart_ap_buttons');
    button_block.data('count', count);
    var price_block = $(this).parent().parent().find('.cart_apt_prices');
    price_block.find('.cart_aptp_price').data('price', price);
    price_block.find('.cart_aptp_price').text(`${numberFormat(price, ' ')} р.`);
    price_block.find('.cart_aptp_old_price').text(`${numberFormat(price_o, ' ')} р.`);
    button_block.find('.cart_apb_amount').text(`${count}шт`);
    addToCart(id, count, 1,'');
  };
  countPrices();
});

$('.cart_s1_product_list').on('click', '.cart_apb_plus', function(){
  var ls_cart_items = JSON.parse(localStorage.getItem('cart'));
  var id = $(this).closest('.cart_added_product').data('id');
  var count = ls_cart_items[id].count;
  var price_current = ls_cart_items[id].price_current;
  var price_old = ls_cart_items[id].price_old;
  count += 1;
  var price = count * price_current;
  var price_o = count * price_old;
  var button_block = $(this).closest('.cart_ap_buttons');
  button_block.data('count', count);
  var price_block = $(this).parent().parent().find('.cart_apt_prices');
  price_block.find('.cart_aptp_price').data('price', price);
  price_block.find('.cart_aptp_price').text(`${numberFormat(price, ' ')} р.`);
  price_block.find('.cart_aptp_old_price').text(`${numberFormat(price_o, ' ')} р.`);
  button_block.find('.cart_apb_amount').text(`${count}шт`);
  addToCart(id, count, 1,'');
  countPrices();
});


//sync removal of items
$('#header-basket__hover').on('click', '.basket__item-close', function(){
  var id = $(this).closest('.basket__item').data('lsid');
  removeItemFromWrapper(id);
  basketIsEmpty();
});


// displaying "basket is empty" message in case it is empty and there is no message present
function basketIsEmpty(){
  var added_products = $('.cart_s1_product_list .cart_added_product').length;
  var empty_message_count = $('.cart_s1_product_list .cart_empty_warning').length;
  if (added_products == 0 && empty_message_count == 0) {
    var product_list = $('.cart_s1_product_list')
    $("<p class='cart_empty_warning'> Ваша корзина пуста.</p>").hide().appendTo(product_list).show('normal');
    $('.cart_s1_product_list h3').hide();
    $('.cart_s1_price_block').hide();
  };
};

// calculating the total price
function countPrices(){
  var global_price = 0;
  var global_old_price = 0;
  var ls_cart_items = JSON.parse(localStorage.getItem('cart'));

  $.each(ls_cart_items, function(index, value){
    var count = ls_cart_items[index].count;
    var price = ls_cart_items[index].price_current;
    var price_old = ls_cart_items[index].price_old;
    global_price += parseInt(price * count);
    global_old_price += parseInt(price_old * count);
  });
  $('.cart_s1pbp_price').text(numberFormat(global_price.toString(), ' ') + ' р.');
  $('.cart_s2pbp_price').text(numberFormat(global_price.toString(), ' ') + ' р.');
  if(global_old_price > 0){
  	var discount = Math.round(global_price * 100 / global_old_price - 100);
    $('.cart_s1pbp_old_price').text(numberFormat(global_old_price.toString(), ' ') + ' р.');
    $('.cart_s2pbp_old_price').text(numberFormat(global_old_price.toString(), ' ') + ' р.');
    $('.cart_s1pbp_discount').text(discount.toString() + '%');
    $('.cart_s2pbp_discount').text(discount.toString() + '%');
    $('.cart_s1pbp_old_price, .cart_s2pbp_old_price').css('display', 'block');
    $('.cart_s1pbp_discount, .cart_s2pbp_discount').css('display', 'flex');
  } else {
    $('.cart_s1pbp_old_price, .cart_s2pbp_old_price, .cart_s1pbp_discount, .cart_s2pbp_discount').css('display', 'none');
  }


  $('.cart_s2pb_deldiscount_sum').text(numberFormat((global_price*0.05).toString(), ' ') + ' р.');

  $('.cart_s2pb_total_price p:nth-child(2) span').text(numberFormat((global_price-global_price*0.05).toString(), ' '));
};

// removing the item from the wrapper cart
  function removeItemFromWrapper(id){
    var victim = $('.cart_s1_product_list').find(`[data-id='${id}']`);
    victim.remove();
  };

  function numberFormat(_number, _sep) {
    _number = Math.round(_number).toString();
    _number = typeof _number != "undefined" && _number > 0 ? _number : "";
    _number = _number.replace(new RegExp("^(\\d{" + (_number.length%3? _number.length%3:0) + "})(\\d{3})", "g"), "$1 $2").replace(/(\d{3})+?/gi, "$1 ").trim();
    if(typeof _sep != "undefined" && _sep != " ") {
        _number = _number.replace(/\s/g, _sep);
    }
    return _number;}


  // body.on('click','.cart_submit_order, .cart_submit_card', function(e){
  //   e.preventDefault;

  //     var cart = getCart();
  //     var strResult = ""; //$(this).hasClass('credit-btn') ? 'КРЕДИТ; ' : '';
  //     if($(this).hasClass('cart_submit_card')) {
  //       $('input[name="content[pay_type]"]').val("card");
  //     }
  //     else {
  //       $('input[name="content[pay_type]"]').val("order");
  //     }

  //     for (i in cart) {
  //       let diameter = '';
  //       if(cart[i].diameter){
  //         diameter = cart[i].diameter;
  //       }
  //       if(cart[i].cap){
  //         cap = cart[i].cap;
  //       }
  //         if (cart[i] && cart[i]['count']) {
  //             strResult += i + ' : ' + cart[i]['count'] + 'шт, Хомуты: ' + colors[cart[i].color1] + ' цвет'
  //             + ', Стойки: ' + colors[cart[i].color2] + ' цвет, диаметр ' + diameter  + ', заглушки ' + cap  + '; \r\n';
  //         }
  //     }
  //     // if (cart.clamps) {
  //     //     for (i in cart.clamps) {
  //     //         if (cart.clamps[i] && cart.clamps[i]['count']) {
  //     //             strResult += 'Хомут ' + i + ' : ' + cart.clamps[i]['count'] + 'шт цвет ' + colors[cart.clamps[i]['color']] + '; \r\n';
  //     //         }
  //     //     }
  //     // }
  //     $('input[name="content[items]"]').val(strResult);
  //     var total = parseInt($('.cart_s2pb_total_price p:last-of-type').html().replace(/\s+/g, ''));
  //     $('input[name="content[total]"]').val(total);
  //     return false;
  //     // $('#form').submit();
  // });




});
