$(document).ready(function() {
  $('.clamps-table-collapse-content-row').on('click', function(){
    $('.clamps-table-collapse-content-row').each(function() {
      $(this).removeClass('clamps-table-collapse-content-row--active');
    })
    $(this).addClass('clamps-table-collapse-content-row--active');
  });
  $('.js-clamps-table-td').on('click', function(){
    $('.js-clamps-table-td').each(function() {
      $(this).removeClass('js-clamps-table-td--active');
    })
    $(this).addClass('js-clamps-table-td--active');
  });  
});